### NAME
Analyzing fleet of instances and using Gitlab runners

### DESCRIPTION
Project Day 3

### INSTRUCTIONS
_Install_

Make sure you have gcc version 11.1.0

cmake minimum required version 3.10

_Usage_
```sh
$ git init
```
```sh
$ git clone git@gitlab.com:tinonum/third-assigment.git
```
```sh
$ mkdir build && cd build
```
```sh
$ cmake../
```
```sh
$ make
```
```sh
$ ./runTests
```
```sh
$ ./main
```

### PROGRAMFAQ
Program takes state of the instance fleet from provided FleetState.txt

Handles provided data and outputs statistics of the fleet to Statistics.txt

### GitlabCI
Program pipeline has three stages BUILD => TEST => DEPLOY

BUILD; builds program using cmake and produces artifact of the program files

TEST; runs unit tests using gtest and produces result.xml artifact

DEPLOY; runs program and produces Statistics.txt artifact

### CONTRIBUTION
Antti = Most of the main.cpp, most of the util.cpp, util.hpp

Tino = Most of the .gitlab-ci.yml, Most of the test.cpp, CMakeLists.txt

Project was made in tight collaboration of two developers.

### BUGS
If you try to read larger file than 21 lines = segmentation fault.

If data comes in different order, analyzing doesn't handle the data correctly.

### ASSUMPTIONS
If we are tasked to build something similar in the future. We would plan the logic differently. 

Also use more object oriented style. During planning, we should have thought more about testing.

During planning, we should have prioritize testing and over all purpose of the project (using gitlab ci and gtest).

### AUTHORS
Tino Nummela | [@tinonum](https://gitlab.com/tinonum)

Antti Lehtosalo | [@mediator246](https://gitlab.com/mediator246)