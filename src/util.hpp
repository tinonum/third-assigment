#pragma once

#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <array>

// Util functions
int analyze_line(std::string lines);
int print(std::string statistics); 