#include "util.hpp"

// M values 0= FULL ones, 1=Empty ones, 2 count of the most filled, 3 how many empty slots they have
std::array<int, 4> M1 = {0, 0, 0, 0};
std::array<int, 4> M2 = {0, 0, 0, 0};
std::array<int, 4> M3 = {0, 0, 0, 0};

std::string m1="M1";
std::string m2="M2";
std::string m3="M3";

// Function to analyze input from file (FleetState.txt) line by line
int analyze_line(std::string lines)
{
    std::stringstream ss(lines);
    std::string host_id;
    std::string instance_type;
    std::string number_of_slots;

    std::getline(ss, host_id, ',');

    int numberofslots;
    int slot_value;

    std::getline(ss, instance_type, ',');
    std::getline(ss, number_of_slots, ',');
    std::stringstream intvalue1(number_of_slots);
    intvalue1 >> numberofslots;

    std::string value;
    int count = 0;

        //check how many slots are filled
        for (int i = 0; i < numberofslots; i++)
        {
            std::getline(ss, value, ',');
            std::stringstream intvalue2(value);
            intvalue2 >> slot_value;

            if (slot_value == 1)
            {
                count++;
            }
            else if (slot_value == 0)
            {

            }
            else if (slot_value > 1 || slot_value < 0)
            {
                return 1;
            }
            else
            {
                return 1;
            }
        }
        //if full then add to M1 first number 1
        if (count == numberofslots)
        {
            if (instance_type == m1)
            {
                M1[0] += 1;
            }
            else if (instance_type == m2)
            {
                M2[0] += 1;
            }
            else if (instance_type == m3)
            {
                M3[0] += 1;
            }
            else
            {
                return 1;
            }

        }
        //if empty then add to second number
        else if (count == 0)
        {
            if (instance_type == m1)
            {
                M1[1] += 1;
            }
            else if (instance_type == m2)
            {
                M2[1] += 1;
            }
            else if (instance_type == m3)
            {
                M3[1] += 1;
            }
            else
            {
                return 1;
            }
        }
        //if neither emtpy nor full then add to third number the amount of empty.
        // M values 0= FULL ones, 1=Empty ones, 2 count of the most filled, 3 how many empty slots they have
        else if (count != 0 && count != numberofslots)
        {
            if (instance_type == m1)
            {
                if (M1[2] >= (numberofslots - count) || M1[2]==0)
                {
                    if (M1[2]==(numberofslots - count))
                    {
                        M1[3]++;
                    }
                    if (M1[2] != (numberofslots - count) )
                    {
                        M1[2] = (numberofslots - count);
                        M1[3] = 1;
                    }
                }
            }
            else if (instance_type == m2)
            {
                if (M2[2] >= (numberofslots - count) || M2[2]==0)
                {
                    if (M2[2]==(numberofslots - count))
                    {
                        M2[3]++;
                    }
                    if (M2[2] != (numberofslots - count) )
                    {
                        M2[2] = (numberofslots - count);
                        M2[3] = 1;
                    }
                }
            }
            else if (instance_type == m3)
            {
                if (M3[2] >= (numberofslots - count) || M3[2]==0)
                {
                    if (M3[2]==(numberofslots - count))
                    {
                        M3[3]++;
                    }
                    if (M3[2] != (numberofslots - count) )
                    {
                        M3[2] = (numberofslots - count);
                        M3[3] = 1;
                    }
                }
            }
            else if (instance_type == m3)
            {
                if (M3[2] >= (numberofslots - count) || M3[2]==0)
                {
                    M3[2] = (numberofslots - count);
                    M3[3]++;
                }
            }
            //error state
            else
            {
                return 1;
            }
        }
        //error state
        else
        {
            return 1;
        }
    return 0;
}

// Function to print statistics into statistics.txt
int print(std::string statistics)
{
    std::ofstream file(statistics);
    if (file.is_open())
    {
        file << "EMPTY:M1=<"<<M1[1]<<">; M2=<"<<M2[1]<<">; M3=<"<<M3[1]<<">; \n";
        file << "FULL:M1=<"<<M1[0]<<">; M2=<"<<M2[0]<<">; M3=<"<<M3[0]<<">; \n";
        file << "MOST FILLED:M1=<"<<M1[3]<<">;<"<<M1[2]<<">; M2=<"<<M2[3]<<">;<"<<M2[2]<<">; M3=<"<<M3[3]<<">;<"<<M3[2]<<">; \n";

        file.close();

        return 0;
    }
    else
    {
        std::cout << "Error in printing the file." << std::endl;
        return 1;
    }
}
