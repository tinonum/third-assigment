#include "util.hpp"
#include <gtest/gtest.h>

// Test to check if given file exist
TEST(UtilLibrary, Exist_SUCCESS)
{
  FILE* source_file = fopen("../FleetState.txt", "r");
  ASSERT_TRUE(source_file != NULL);
};

// Test to check if give file exist (ASSERT_FALSE, because it shouldn't exist)
TEST(UtilLibrary, Exist_FAIL)
{
  FILE* source_file = fopen("../Random.txt", "r");
  ASSERT_FALSE(source_file != NULL);
};

// Test to analyze if input from source file is correct
TEST(UtilLibrary, Analyze_True)
{
  ASSERT_EQ(0, analyze_line("10,M1,4,0,0,0,0"));
  ASSERT_EQ(0, analyze_line("93,M3,14,0,0,1,1,1,1,1,0,0,1,1,1,1,1"));
  ASSERT_EQ(0, analyze_line("82,M2,7,1,0,0,0,0,0,1"));
}

// Test to analyze if input from source file is correct (ASSERT_EQ(1) so it is supposed to fail)
TEST(UtilLibrary, Analyze_false)
{
  ASSERT_EQ(1, analyze_line("10,M100,4,x,sdsdsd,0,0"));
  ASSERT_EQ(1, analyze_line("913,M3,114,0,20,1,1,1,,1,0,0,1,11,1,1,1"));
  ASSERT_EQ(1, analyze_line("82,M2,7,1,41,0,x,0,0,1"));
}

// Test to try printing main.cpp output (test for statistics.txt)
TEST(UtilLibrary, Print_Stats)
{
  ASSERT_EQ(0, print("dasjdas.log"));
  ASSERT_EQ(0, print("askdasd.txt"));
};

// Google test driver
int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
