// AUTHORS: Antti Lehtosalo, Tino Nummela
// Project Day 3
// Analyzing fleet of instances and using Gitlab runners

#include "util.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <array>

// Initialize array of strings
std::string line[21];

// Function to read file
void read_file(std::string readfile)
{
    std::ifstream file(readfile);
    int num = 0;

    if (file.is_open())
    {
        while (std::getline(file, line[num]))
        {
            num++;
        }
    }
    else
    {
        std::cout << "error in opening file";
    }
    file.close();
}

// Driver code
int main()
{
    read_file("../FleetState.txt");

    for (int i=0; i<21; i++)
    {
        analyze_line(line[i]);
    }
    print("../Statistics.txt");

    return 0;
}
// End of Program